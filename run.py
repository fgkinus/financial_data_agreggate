from flask_jwt_extended import JWTManager
from flask_restplus import Api
from flask import Flask

app = Flask(__name__)
if __name__ == '__main__':
    app.run()
